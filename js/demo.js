window.onload = function(){
	// 顶部的透明度
	headScroll();
	// 时间的显示
	downTime();
	// 轮播图
	banner();
}


function headScroll(){
	// 获取通栏
	var jd_header = document.querySelector(".jd_head");
	// 获取顶部的高度
	var jd_main = document.querySelector(".slideshow").scrollHeight;
	window.onscroll = function(){
		var top = window.pageYOffset;
		var opacity = top / jd_main;
		if(opacity > 1){
			opacity = 1;
		}
		jd_header.style.background = "rgba(201,21,35,"+opacity+")";
	}
}



function downTime(){
	var times = document.querySelectorAll(".main .main_content .main_top ul li");
	// console.log(times);
	/*
		假设时间为5 * 60 * 60
	*/
	var time = 25 * 60 * 60;
	var timer = setInterval(function(){
		if(time <= 0){
			clearInterval(timer);
		}
		time--;
		var h = Math.floor(time/3600);
		var m = Math.floor(time%3600/60);
		var s = Math.floor(time%60);
		// console.log(h+":"+m+":"+s);
		times[0].innerHTML = Math.floor(h/10);
		times[1].innerHTML = Math.floor(h%10);
		times[3].innerHTML = Math.floor(m/10);
		times[4].innerHTML = Math.floor(m%10);
		times[6].innerHTML = Math.floor(s/10);
		times[7].innerHTML = Math.floor(s%10);
	},1000)
}


function banner(){
	/*
		首先定时器轮播
		1每个图片的宽度
		2定时器
	*/
	// 获取ul
	var ulImages = document.querySelector(".slide .slideshow");
	// 获取图片的宽度
	var imageWidth = document.body.offsetWidth;
	// 获取图片的索引
	var imagesIndex = document.querySelectorAll(".slide .slideshow li");
	// ul的长度
	// var ulWidth = imagesLength.length * imageWidth;
	// 获取小圆点
	var current = document.querySelectorAll(".slide .origin li");
	// console.log(current);
	var index = 1;
	var timer = setInterval(function(){
		index++;
		ulImages.style.transition = "all .3s";
		ulImages.style.transform = "translateX(" + index*imageWidth*-1 +"px)";
	},1000)


	/*添加事件过渡事件*/
	ulImages.addEventListener("webkitTransitionEnd",function(){
		// console.log("过渡结束");
		if(index > 8){
			index = 1;
			ulImages.style.transition = "";
		}else if(index < 1){
			index = 8;
			ulImages.style.transition = "";
		}
		for (var i = 0; i < current.length; i++) {
			current[i].className = '';
		}
		current[index-1].className = 'current';
		ulImages.style.transform = "translateX(" + index*imageWidth*-1 +"px)";
	})
	

	// 添加滑动事件
	var distance = 0;//滑动的距离
	var startDis = 0;//开始的屏幕像素点
	var endDis = 0;//结束的屏幕像素点

	// 触摸开始
	ulImages.addEventListener("touchstart",function(e){
		e = event || window.event;
		// console.log(e);
		clearInterval(timer);
		ulImages.style.transition = "";
		startDis = e.touches[0].clientX;//获取开始值
	})
	// 触摸中
	ulImages.addEventListener("touchmove",function(e){
		distance = e.touches[0].clientX - startDis;//移动的距离
		ulImages.style.transform = "translateX(" + (distance+index*imageWidth*-1) +"px)";
	})
	// 触摸结束
	ulImages.addEventListener("touchend",function(e){
		// 触摸结束的时候让图片回来
		var maxDis = imageWidth/3;
		// 判断滑动的距离
		if(Math.abs(distance) > maxDis){
			if(distance > 0){//右划
				index--;
			}else{//左滑
				index++;
			}
			ulImages.style.transition = "all .3s";
			//过去一整页
			ulImages.style.transform = "translateX(" + index*imageWidth*-1 +"px)";
		}else{
			ulImages.style.transition = "all .3s";
			ulImages.style.transform = "translateX(" + index*imageWidth*-1 +"px)";
		}

		//添加过渡效果
		timer = setInterval(function(){
			index++;
			ulImages.style.transition = "all .3s";
			ulImages.style.transform = "translateX(" + index*imageWidth*-1 +"px)";
		},1000)
	})

}